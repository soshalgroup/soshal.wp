/**
 * gulpfile.js
 *
 * Runs dev and dist tasks for this project.
 *
 * Usage:
 * 'gulp'      Starts a watcher for development.
 * 'gulp dist' Builds the dist version (usually for a new release).
 * 'gulp test' Runs testing tools like the a11y checker.
 */





/**
 * Gulp configuration.
 *
 * Loads all required plugins, as well as options for those plugins.
 */
var gulp        = require('gulp');
var plugins     = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var chalk       = require('chalk');
var pkg         = require('./package.json');


// Options for plumber.
var plumber = {

    options: {
        errorHandler: errorHandler
    }

};


// Options for Sass lint.
var sass_lint = {

    input: [
        'wp-content/themes/' + pkg.name + '/sass/**/*.scss',
        '!wp-content/themes/' + pkg.name + '/sass/vendor/**/*.scss'
    ],

    options: {
        config: 'scss-lint.yml',
        maxBuffer: 1024000
    }

}


// Options for Sass build.
var sass_build = {

    input: [
        'wp-content/themes/' + pkg.name + '/sass/**/*.scss'
    ],

    output: 'wp-content/themes/' + pkg.name + '/',

    options: {
        outputStyle: 'compressed'
    }

};


// Options for JS lint.
var js_lint = {

    input: [
        'wp-content/themes/'  + pkg.name + '/js/**/*.js',
        '!wp-content/themes/' + pkg.name + '/js/vendor/**/jquery*.js',
        '!wp-content/themes/' + pkg.name + '/js/vendor/**/modernizr*.js',
        '!wp-content/themes/' + pkg.name + '/js/dist/**/*.js'
    ]

};


// Options for JS build.
var js_build = {

    input: [
        'wp-content/themes/'  + pkg.name + '/js/vendor/**/modernizr*.js',
        'wp-content/themes/'  + pkg.name + '/js/tools/**/*.js',
        'wp-content/themes/'  + pkg.name + '/js/plugins/**/*.js',
        'wp-content/themes/'  + pkg.name + '/js/scripts.js',
        '!wp-content/themes/' + pkg.name + '/js/tools/**/*_dev*.js'
    ],

    output: 'wp-content/themes/' + pkg.name + '/js/dist/'

};


// Options for Autoprefixer.
var autoprefixer = {

    options: ['last 2 versions', 'ie 9']

};


// Options for the a11y audit.
var a11y = {

    pages: [
        'http://192.168.33.10',
        'http://192.168.33.10/sample-page/',
        'http://192.168.33.10/some-test-page/'
    ]

};


// Options for the accessibility audit.
var wcag = {

    options: {
        urls: [
            'http://192.168.33.10',
            'http://192.168.33.10/sample-page/',
            'http://192.168.33.10/some-test-page/'
        ],
        force: true,
        accessibilityLevel: 'WCAG2AA',
        accessibilityrc: true,
        reportLevels: {
            notice: false,
            warning: false,
            error: true
        },
        domElement: true
    }

};





/**
 * Errorhandler.
 *
 * Takes any gulp errors and outputs them to the terminal.
 *
 * @param {object} err The error object.
 */
function errorHandler(err) {

    console.log('');
    console.log('  ' + chalk.red('[') + 'error' + chalk.red(']') + ' · ' + chalk.red('in ') + err.plugin);
    console.log('  ' + chalk.red('[') + 'error' + chalk.red(']') + ' · ' + err.lineNumber + ':' + err.message);
    console.log('  ' + chalk.red('[') + 'error' + chalk.red(']') + ' · ' + err.fileName);
    console.log('');

    this.emit('end');

}





/**
 * Dev tasks.
 *
 * A collection of tasks required for development.
 */

// Main watcher.
gulp.task('dev:watch', function() {

    console.log('');
    console.log('  ' + chalk.cyan('[') + 'dev' + chalk.cyan(']') + ' · ' + chalk.cyan('watching') + ' project files');
    console.log('');

    // Let livereload listen for changes.
    plugins.livereload.listen();

    // Watch Sass files.
    gulp.watch(sass_build.input, ['dev:sass-lint', 'dev:sass-compile']);

    // Watch JavaScript files.
    gulp.watch(js_build.input, ['dev:js-lint', 'dev:js-compile']);

});


// Lints all Sass files that are uncached.
gulp.task('dev:sass-lint', function() {

    console.log('');
    console.log('  ' + chalk.cyan('[') + 'dev' + chalk.cyan(']') + ' · ' + chalk.cyan('linting') + ' .scss files');
    console.log('');

    return gulp.src(sass_lint.input)
        //.pipe(plugins.cached('sass-lint'))
        .pipe(plugins.plumber(plumber.options))
        .pipe(plugins.scssLint(sass_lint.options));

});


// Parse Sass and autoprefix the generated CSS.
gulp.task('dev:sass-compile', function() {

    console.log('');
    console.log('  ' + chalk.cyan('[') + 'dev' + chalk.cyan(']') + ' · ' + chalk.cyan('compiling') + ' .scss files');
    console.log('');

    return gulp.src(sass_build.input)
        .pipe(plugins.plumber(plumber.options))
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sass(sass_build.options))
        .pipe(plugins.autoprefixer(autoprefixer.options))
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest(sass_build.output))
        .pipe(plugins.livereload());

});


// Lints all JavaScript files that are uncached.
gulp.task('dev:js-lint', function() {

    console.log('');
    console.log('  ' + chalk.cyan('[') + 'dev' + chalk.cyan(']') + ' · ' + chalk.cyan('linting') + ' .js files');
    console.log('');

    return gulp.src(js_lint.input)
        .pipe(plugins.cached('js-lint'))
        .pipe(plugins.plumber())
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('jshint-stylish'))

});


// Concat and sourcemap JavaScripts.
gulp.task('dev:js-compile', function() {

    console.log('');
    console.log('  ' + chalk.cyan('[') + 'dev' + chalk.cyan(']') + ' · ' + chalk.cyan('compiling') + ' .js files');
    console.log('');

    return gulp.src(js_build.input)
        .pipe(plugins.plumber())
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.uglify())
        .pipe(plugins.concat('scripts.min.js'))
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest(js_build.output))
        .pipe(plugins.livereload());

});





/**
 * Dist tasks.
 *
 * A collection of tasks required for the dist build.
 */

// Lints all Sass files.
gulp.task('dist:sass-lint', function() {

    console.log('');
    console.log('  ' + chalk.magenta('[') + 'dist' + chalk.magenta(']') + ' · ' + chalk.magenta('linting') + ' .scss files');
    console.log('');

    return gulp.src(sass_lint.input)
        .pipe(plugins.plumber(plumber.options))
        .pipe(plugins.scssLint(sass_lint.options));

});


// Parse Sass and autoprefix the generated CSS.
gulp.task('dist:sass-compile', function() {

    console.log('');
    console.log('  ' + chalk.magenta('[') + 'dist' + chalk.magenta(']') + ' · ' + chalk.magenta('compiling') + ' .scss files');
    console.log('');

    return gulp.src(sass_build.input)
        .pipe(plugins.plumber(plumber.options))
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sass(sass_build.options))
        .pipe(plugins.autoprefixer(autoprefixer.options))
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest(sass_build.output))
        .pipe(plugins.livereload());

});


// Lints all JavaScript files.
gulp.task('dist:js-lint', function() {

    console.log('');
    console.log('  ' + chalk.magenta('[') + 'dist' + chalk.magenta(']') + ' · ' + chalk.magenta('linting') + ' .js files');
    console.log('');

    return gulp.src(js_lint.input)
        .pipe(plugins.plumber())
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('jshint-stylish'))

});


// Concat and sourcemap JavaScripts.
gulp.task('dist:js-compile', function() {

    console.log('');
    console.log('  ' + chalk.magenta('[') + 'dist' + chalk.magenta(']') + ' · ' + chalk.magenta('compiling') + ' .js files');
    console.log('');

    return gulp.src(js_build.input)
        .pipe(plugins.plumber())
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.uglify())
        .pipe(plugins.concat('scripts.min.js'))
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest(js_build.output))
        .pipe(plugins.livereload());

});





/**
 * Test tasks.
 *
 * A collection of tasks to assist with testing things like accessibility.
 */

// Check URLs against a11y standards and make a report.
gulp.task('test:a11y', function() {

    var now = Date.now();

    console.log('');
    console.log('  ' + chalk.cyan('[') + 'dev' + chalk.cyan(']') + ' · ' + chalk.cyan('auditing') + ' a11y accessibility (report)');
    console.log('  ' + chalk.cyan('[') + 'dev' + chalk.cyan(']') + ' · ' + chalk.cyan('reporting') + ' to .tests/a11y/' + now + '_a11y_audit.txt');
    console.log('');

    return plugins.run('a11y ' + a11y.pages.join(' ')).exec()
        .pipe(plugins.plumber())
        .pipe(plugins.rename(now + '_a11y_audit.txt'))
        .pipe(gulp.dest('.tests/a11y'));

});


// Check URLs against a11y standards and logs the result instead of generating
// a report.
gulp.task('test:a11y-log', function() {

    console.log('');
    console.log('  ' + chalk.cyan('[') + 'dev' + chalk.cyan(']') + ' · ' + chalk.cyan('auditing') + ' a11y accessibility (console)');
    console.log('');

    return plugins.run('a11y ' + a11y.pages.join(' ')).exec();

});


// Check URLs against WCAG 2.0 standards and reports to the console.
gulp.task('test:wcag', function() {

    console.log('');
    console.log('  ' + chalk.cyan('[') + 'dev' + chalk.cyan(']') + ' · ' + chalk.cyan('checking') + ' ' + wcag.options.accessibilityLevel + ' compliancy');
    console.log('');

    return plugins.accessibility(wcag.options);

});


// Access test
gulp.task('test:acc', function() {

    console.log('');
    console.log('  ' + chalk.cyan('[') + 'dev' + chalk.cyan(']') + ' \u2219 ' + chalk.cyan('auditing') + ' a11y accessibility');
    console.log('');

    return plugins.accessibility({
        urls: ['http://192.168.33.10'],
        force: true,
        accessibilityLevel: 'WCAG2A'
        //reportType: 'json',
        //reportLocation: '.audit',
    });

});





/**
 * Gulp commands.
 *
 * Expose certain tasks or sequences to the CLI.
 */
gulp.task('default', ['dev:watch']);
gulp.task('dist', ['dist:sass-lint', 'dist:sass-compile', 'dist:js-lint', 'dist:js-compile']);
gulp.task('test', ['test:a11y', 'test:wcag']);
