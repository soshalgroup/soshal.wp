<?php

/**
 * footer.php
 *
 * The global footer for this theme.
 */

?>

    <footer class="footer footer--global" role="contentinfo">

      <p class="copyright"><small>Copyright <?php bloginfo("name"); ?>.</small></p>

    </footer>

    <?php wp_footer(); ?>

  </body>
</html>
