#!/usr/bin/env bash

# path="/var/www/public/"

sudo apt-get update

echo "--- Installing wp-cli ---"
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wpcli

echo "--- Downloading Latest Wordpress ---"
sudo -u vagrant -i -- wpcli core download --path="/var/www/public/"

echo "--- Creating Wordpress Database ---"
sudo -u vagrant -i -- wpcli db create --path="/var/www/public/"

echo "--- Creating Wordpress Database Tables ---"
sudo -u vagrant -i -- wpcli core install --path="/var/www/public/" --url="dev.vagrant.ca" --title="Test Site" --admin_user="soshal" --admin_password="hulkh0gan" --admin_email="it.ops@soshal.ca"

echo "--- Installing Wordpress default plugins ---"
sudo -u vagrant -i -- wpcli plugin install wp-accessibility --path="/var/www/public/"
sudo -u vagrant -i -- wpcli plugin install wordpress-seo --path="/var/www/public/"
sudo -u vagrant -i -- wpcli plugin install display-widgets --path="/var/www/public/"
sudo -u vagrant -i -- wpcli plugin install advanced-custom-fields --path="/var/www/public/"
sudo -u vagrant -i -- wpcli plugin install duplicator --path="/var/www/public/"
sudo -u vagrant -i -- wpcli plugin install wp-optimize --path="/var/www/public/"
sudo -u vagrant -i -- wpcli plugin install better-wp-security --path="/var/www/public/"

echo "--- Plugins have been installed but still need to be manually activated ---"