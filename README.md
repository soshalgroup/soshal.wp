# soshal.wp

Soshal's base Wordpress theme.

* Project: [soshal.wp](https://bitbucket.org/soshalgroup/soshal.wp/overview)
* Author:  [Soshal](http://www.soshal.ca)

## Instructions

The framework is automatically included in our Dokku images, so there's usually
no need to download it separately.

If you ever need the theme for some other reason, you should download the
[latest release](https://bitbucket.org/soshalgroup/soshal.wp/downloads/soshal.wp_latest.zip)
