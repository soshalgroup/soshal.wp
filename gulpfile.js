/**
 * gulpfile.js
 *
 * Runs dev and dist tasks for this project.
 */





// Gulp configuration.
var fs          = require('fs');
var path        = require('path');

var gulp        = require('gulp');
var plugins     = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var chalk       = require('chalk');

var s3_config   = require('./s3_config.json');
var s3          = plugins.s3Upload({
    accessKeyId:     s3_config.ID,
    secretAccessKey: s3_config.KEY
});

var pkg         = require('./package.json');
var dirs        = pkg['soshal.wp-config'].directories;


// Options for plumber.
var plumber = {

    options: {
        errorHandler: errorHandler
    }

};





/**
 * Error handler.
 *
 * Takes any gulp errors and outputs them to the terminal.
 *
 * @param object err The error object.
 */
function errorHandler(err) {

    console.log('');
    console.log('  ' + chalk.red('[') + 'error' + chalk.red(']') + ' \u2219 ' + chalk.red('in ') + err.plugin);
    console.log('  ' + chalk.red('[') + 'error' + chalk.red(']') + ' \u2219 ' + err.lineNumber + ':' + err.message);
    console.log('  ' + chalk.red('[') + 'error' + chalk.red(']') + ' \u2219 ' + err.fileName);
    console.log('');

    this.emit('end');

}





// Builds the dist files.
gulp.task('dist:build', function(done) {

    console.log('');
    console.log('  ' + chalk.cyan('\u2219  ') + chalk.white('starting ' + chalk.gray('dist')));
    console.log('');

    runSequence(
        'dist:remove-folder',
        'dist:create-folder',
        ['dist:archive-latest', 'dist:archive-versioned'],
        'dist:upload',
    done);

});


// Removes the dist folder.
gulp.task('dist:remove-folder', function(done) {

    console.log('');
    console.log('  ' + chalk.magenta('[') + 'dist' + chalk.magenta(']') + ' \u2219 ' + chalk.magenta('cleaning') + ' dist folder');
    console.log('');

    require('del')([
        dirs.dist
    ], done);

});


// Creates the dist folder.
gulp.task('dist:create-folder', function() {

    console.log('');
    console.log('  ' + chalk.magenta('[') + 'dist' + chalk.magenta(']') + ' \u2219 ' + chalk.magenta('creating') + ' dist folder');
    console.log('');

    fs.mkdirSync(dirs.dist, '0755');

});


// Creates the latest archive.
gulp.task('dist:archive-latest', function(done) {

    console.log('');
    console.log('  ' + chalk.magenta('[') + 'dist' + chalk.magenta(']') + ' \u2219 ' + chalk.magenta('archiving') + ' WordPress theme');
    console.log('');

    return gulp.src([
            '*',
            '!*.php',
            '!*.html',
            '!*.config',
            '!**/*.DS_Store',
            '!license.txt',
            '!Dockerfile',
            '!Vagrantfile',
            '!setup.sh',
            '!bootstrap.sh',
            'wp-content/themes/soshal_theme/**/*.*',
            '!wp-content/themes/soshal_theme/sass/**/*.*'
        ], { 'cwd': dirs.wp, base: dirs.wp, dot: true })
        .pipe(plugins.plumber(plumber.options))
        .pipe(plugins.zip(pkg.name + '_latest.zip'))
        .pipe(gulp.dest(dirs.dist));

});


// Creates the versioned archive.
gulp.task('dist:archive-versioned', function(done) {

    console.log('');
    console.log('  ' + chalk.magenta('[') + 'dist' + chalk.magenta(']') + ' \u2219 ' + chalk.magenta('archiving') + ' WordPress theme');
    console.log('');

    return gulp.src([
            '*',
            '!*.php',
            '!*.html',
            '!*.config',
            '!**/*.DS_Store',
            '!license.txt',
            '!Dockerfile',
            '!Vagrantfile',
            '!setup.sh',
            '!bootstrap.sh',
            'wp-content/themes/soshal_theme/**/*.*',
            '!wp-content/themes/soshal_theme/sass/**/*.*'
        ], { 'cwd': dirs.wp, base: dirs.wp, dot: true })
        .pipe(plugins.plumber(plumber.options))
        .pipe(plugins.zip(pkg.name + '_v' + pkg.version + '.zip'))
        .pipe(gulp.dest(dirs.dist));

});


// Uploads archives to the s3 bucket.
gulp.task('dist:upload', function(done) {

    console.log('');
    console.log('  ' + chalk.magenta('[') + 'dist' + chalk.magenta(']') + ' \u2219 ' + chalk.magenta('uploading') + ' archives');
    console.log('');

    return gulp.src([
        'dist/**'
        ])
        .pipe(plugins.plumber(plumber.options))
        .pipe(s3({
            Bucket: 'soshal-resources/soshal-theme',
            ACL:    'public-read'
        }));

});





// Gulp commands.
gulp.task('default', ['dist:build']);
gulp.task('dist', ['dist:build']);
